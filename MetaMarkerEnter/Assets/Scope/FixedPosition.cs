﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedPosition : MonoBehaviour {


    public Vector3 offset;
    public float smoothTresh = 0.005f;
    public Transform ref_camera;

    private Vector3 prev_position;

   
	// Use this for initialization
	void Start () {
        prev_position = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 curr_position = ref_camera.position;
        Vector3 curr_offset = ref_camera.rotation * offset;
        transform.rotation = ref_camera.rotation;

        if (Vector3.Distance(prev_position, curr_position) > smoothTresh)
        {
            transform.position = curr_position + curr_offset;
        }
    }
}
