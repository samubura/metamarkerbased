﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Meta;
using Meta.Plugin;
using Meta.Buttons;
using Meta.Interop.Buttons;

public class ClickConfirm : MonoBehaviour, IOnMetaButtonEvent
{


    public GameObject prefab;
    private bool clicked = false;
    private GameObject obj;
    // Use this for initialization
    void Start()
    {
        GameObject.FindGameObjectWithTag("metacamera").GetComponent<MetaCompositor>().EnableHandOcclusion = false;
        GameObject.Find("BigLight").GetComponent<Light>().enabled = false;
        GameObject.Find("FrontLight").GetComponent<Light>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            OnButtonClick();
        }
    }

    public void OnButtonClick()
    {
        if (!clicked)
        {
            obj = GameObject.Instantiate(prefab, this.gameObject.transform.position, this.gameObject.transform.rotation);
            this.GetComponentInChildren<MeshRenderer>().enabled = false;
            GameObject.FindGameObjectWithTag("metacamera").GetComponent<MetaCompositor>().EnableHandOcclusion = true;
            GameObject.Find("BigLight").GetComponent<Light>().enabled = true;
            GameObject.Find("FrontLight").GetComponent<Light>().enabled = false;
            clicked = true;
        } else
        {
            clicked = false;
            GameObject.Destroy(obj);
            this.GetComponentInChildren<MeshRenderer>().enabled = true;
            GameObject.FindGameObjectWithTag("metacamera").GetComponent<MetaCompositor>().EnableHandOcclusion = false;
            GameObject.Find("BigLight").GetComponent<Light>().enabled = false;
            GameObject.Find("FrontLight").GetComponent<Light>().enabled = true;
        }
    }

    public void OnMetaButtonEvent(MetaButton button)
    {
        // If we received a button event, ensure it's the correct type of button press.
        if (button != null)
        {
            if (button.Type != ButtonType.ButtonCamera)
            {
                return;
            }

            if (button.State != ButtonState.ButtonShortPress)
            {
                return;
            }
        }

        if (!this.enabled)
        {
            Debug.LogWarning("Script is not enabled");
            return;
        }
        OnButtonClick();
    }

}
