Meta Marker Based
=================

Questo repository contiene un progetto di esempio per l'integrazione di una registrazione tramite Marker in Meta2.

La registrazione � effettuata andando a sovrapporre un marker virtuale su quello reale ed utilizzando una conferma da parte dell'utente, tramite bottone integrato nel visore per settare l'origine del sistema di riferimento al centro del marker.

Per una migliore prestazione si consiglia di mantenere disabilitata la feature di relocalizzazione di Meta2 ed effettuare lo scan preliminare dell'ambiente all'inizio di ogni esecuzione.

**Attenzione**: il marker virtuale deve essere impostato della stessa dimensione di quello reale.